using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;
using Android.Util;
using Java.Util;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace WeatherApp.AL.Services.GeoService
{
    [Service]
    [IntentFilter(new String[] { "WeatherApp.AL.Services.GeoService.GeoService" })]
    public class GeoService : Service, Android.Locations.ILocationListener
    {
        private Messenger SvcMessanger;        
        private LocationManager LocMgr;
        private string locationProvider;
        public Messenger WeatherSvcMessanger { get; set; }

        public Messenger UIMessanger { get; set; }

        public bool ItsTimeToLocalUpdate { get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }

        public GeoService()
        {
            Log.Debug("______________________________", "Service created");
            this.SvcMessanger = new Messenger(new GeoServiceHandler(this));
        }        

        public override void OnDestroy()
        {
            base.OnDestroy();
        }

        public Location LocationRequest()
        {
            return null;
        }

        public void StartListenLocator(Criteria criteriaConfig, float distance = 3000, long minTime = 60000 )
        {
            if (criteriaConfig == null)
            {
                throw new ArgumentNullException("Location criteria cannot be null.");
            }

            this.LocMgr = GetSystemService(Context.LocationService) as LocationManager;            
            this.locationProvider = this.LocMgr.GetBestProvider(criteriaConfig, true);            
            switch (this.locationProvider)
            {
                case Constants.GEOSVC_PROVIDER_GPS:
                    {
                       // this.LocMgr.RequestLocationUpdates(locationProvider, minTime, distance, this, null);                        
                        this.LocMgr.RequestLocationUpdates(locationProvider, 1000, 0, this, null);                        
                        break;
                    }
                case Constants.GEOSVC_PROVIDER_NETWORK:
                    {
                       // this.LocMgr.RequestLocationUpdates(locationProvider, minTime, 0, this);
                        this.LocMgr.RequestLocationUpdates(locationProvider, 1000, 0, this);
                        break;
                    }
            }
            
        }        

        public void StopListenLocator()
        {            
        }       

        public override IBinder OnBind(Intent intent)
        {
            Log.Debug("______________________________", "Service Binded");
            return SvcMessanger.Binder;
        }

        public override bool OnUnbind(Intent intent)
        {
            return base.OnUnbind(intent);
        }

        public void OnLocationChanged(Location location)
        {
            if (this.UIMessanger != null)
            {
                this.GetWeather(location);
            }
        }

        private async void GetWeather(Location location)
        {
            var core = new WeatherCore.WeatherCore();
            var weather = await core.GetWeatherByCoordinates(location.Latitude, location.Longitude, "00444cc7c51e77bc3f199cc5fa9c7bb7");            
            if (this.UIMessanger != null)
            {
                var msg = Message.Obtain();
                msg.What = Constants.UI_MSG_CURRENTFORECAST;
                var databundle = new Bundle();
                databundle.PutString(Constants.WEATHER_CURRENT_FORECAST, weather);
                msg.Data = databundle;
                this.UIMessanger.Send(msg);    
            }
        }       

        public void OnProviderDisabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnProviderEnabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {
            throw new NotImplementedException();
        }


    }
}