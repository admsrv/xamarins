﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace WeatherAPP
{
    [Activity(Label = "WeatherAPP", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            Button button = FindViewById<Button>(Resource.Id.MyButton);
            button.Click += button_Click;
            Button button2 = FindViewById<Button>(Resource.Id.button2);           
            button2.Click += delegate {
                var d = App.SS.Resolve<WeatherAppServicesConnector>();
                d.StopLocationService();
            };

            Button button3 = FindViewById<Button>(Resource.Id.button3);
            button3.Click += button3_Click;
        }

        void button3_Click(object sender, EventArgs e)
        {
            var d = App.SS.Resolve<WeatherAppServicesConnector>();
            var msg = Message.Obtain();
            msg.What = Constants.GEOSVC_MSG_GEO_STARTLOCATION;
            var msgBundle = new Bundle();
            msgBundle.PutString(Constants.KEY_LOCATOR_ACCURACY, Constants.VALUE_LOCATOR_ACCURACY_FINE);
            msgBundle.PutString(Constants.KEY_LOCATOR_POWER, Constants.VALUE_LOCATOR_POWER_HIGH);
            msg.Data = msgBundle;
            d.GeoConnection.GeoServiceInbox.Send(msg);
        }

        void button_Click(object sender, EventArgs e)
        {
            var d = App.SS.Resolve<WeatherAppServicesConnector>();
            d.StartGeoSvc();
            //var optionsBundle = new Bundle();
            //optionsBundle.PutString(Constants.KEY_LOCATOR_ACCURACY, Constants.VALUE_LOCATOR_ACCURACY_FINE);
            //optionsBundle.PutString(Constants.KEY_LOCATOR_POWER, Constants.VALUE_LOCATOR_POWER_HIGH);
            //var message= Message.Obtain(null, Constants.GEOSVC_MSG_GEO_STARTLOCATION);
            //message.Data = optionsBundle;
            //d.GeoConnection.GeoServiceInbox.Send(message);
        }
    }
}

