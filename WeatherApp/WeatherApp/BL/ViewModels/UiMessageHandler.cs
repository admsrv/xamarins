using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace WeatherApp.BL.ViewModels
{
    public class UiMessageHandler : Handler
    {
        private MainActivity _activity;

        public UiMessageHandler(MainActivity mainActivity)
        {
            this._activity = mainActivity;
        }

        public override void HandleMessage(Message msg)
        {
            switch (msg.What)
            {
                case Constants.UI_MSG_CURRENTFORECAST:
                    {
                        this._activity.UpdateCurrentForecast(msg.Data);
                        break;
                    }
                default:
                    {
                        base.HandleMessage(msg);
                        break;
                    }
            }
            
        }
    }
}