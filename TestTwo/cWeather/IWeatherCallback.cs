﻿namespace cWeather
{
    using System;
    using Android.App;
    using Android.Content;
    using Android.Runtime;
    using Android.Views;
    using Android.Widget;
    using Android.OS;
    using Android.Locations;
    using Android.Util;
    using Android.Content.Res;

    public interface IWeatherCallback
    {
        Criteria LocationCriteria { get; set; }
    }
}