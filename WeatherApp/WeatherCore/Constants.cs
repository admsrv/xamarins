﻿#define Weather2_5
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherCore
{
    public class Constants
    {
        #region Openweathermap2.5
#if Weather2_5
       public const string URL_OPENWEATHERMAPAPI = "http://api.openweathermap.org/data/2.5/";
       public const string APIQUERY_BYGEOCORD = "weather?lat={0}&lon={1}&APPID={2}";
#endif
        #endregion
    }
}
