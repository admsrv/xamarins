namespace TestTwo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Android.App;
    using Android.Content;
    using Android.OS;
    using Android.Runtime;
    using Android.Views;
    using Android.Widget;

    interface IWorkerFunctionality
    {
        TextView DisplayClicksLabel{get;set;}
        void IncClick();
        int ClicksCount { get; set; }
        void ResetClicks();
        void UpdateLabel();
        void SaveSettings();
    }
}