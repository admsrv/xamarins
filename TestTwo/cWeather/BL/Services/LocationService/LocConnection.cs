using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace cWeather
{
    public class LocConnection : Java.Lang.Object, IServiceConnection
    {
        private LocBinder _binder;
        public event ServiceConnectedHandler ServiceConnected;
        public LocConnection(LocBinder bnd)
        {
            this._binder = bnd;
        }

        public void OnServiceConnected(ComponentName name, IBinder service)
        {
            LocBinder serviceBinder = service as LocBinder;

            if (serviceBinder != null)
            {
                this._binder = serviceBinder;
                this._binder.IsBound = true;
                if (this.ServiceConnected != null)
                {
                    this.ServiceConnected(this, new ServiceConnectedEventArgs() { Binder = service });
                }
            }
        }

        public void OnServiceDisconnected(ComponentName name)
        {
            throw new NotImplementedException();
        }
    }
}