using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using WeatherApp.AL.Services.GeoService;
using System.Threading.Tasks;

namespace WeatherApp
{
    [Application]
    public class App : Application
    {
        public static TinyIoC.TinyIoCContainer iocContainer;
        public App(IntPtr handle, JniHandleOwnership transfer)
            : base(handle, transfer)
        {
            iocContainer = new TinyIoC.TinyIoCContainer();
            iocContainer.Register<WeatherAppServicesConnector>().AsSingleton();
        }

        public override void OnCreate()
        {
            base.OnCreate();          
        }
    }
}