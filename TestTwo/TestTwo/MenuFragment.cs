// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MenuFragment.cs" company="AltexSoft">
//  sergiy.chepur@altexsoft.com 
//  � 2015  
// </copyright>
// <summary>
//   Represents the Menu fragment of Clicker app.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestTwo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Android.App;
    using Android.Content;
    using Android.OS;
    using Android.Runtime;
    using Android.Util;
    using Android.Views;
    using Android.Widget;


    /// <summary>
    ///  Represents the Menu fragment of Clicker app.
    /// </summary>
    public class MenuFragment : Fragment
    {

        public const string HidenStateFlagConst = "HidenState";

        private IWorkerFunctionality _menuClickControl;     

        public override void OnAttach(Activity activity)
        {
            base.OnAttach(activity);
            if (activity is IWorkerFunctionality)
            {
                this._menuClickControl = (IWorkerFunctionality)activity;
            }
        }

        public override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            if (this.IsVisible)
            {
                outState.PutBoolean(MenuFragment.HidenStateFlagConst, false);
            }
            else
            {
                outState.PutBoolean(MenuFragment.HidenStateFlagConst, true);
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var menuView = inflater.Inflate(Resource.Layout.layout_menu, container, false);
            if (savedInstanceState != null)
            {
                switch (savedInstanceState.GetBoolean(MenuFragment.HidenStateFlagConst))
                {
                    case true:
                        {
                            menuView.Visibility = ViewStates.Gone;
                            break;
                        }
                    case false:
                        {
                            menuView.Visibility = ViewStates.Visible;
                            break;
                        }
                }
            }

            var exitButton = menuView.FindViewById<Button>(Resource.Id.exitbutton);
            exitButton.Click += delegate {
                this._menuClickControl.SaveSettings();
                Activity.Finish();
            };
            var resetButton = menuView.FindViewById<Button>(Resource.Id.clearbutton);
            resetButton.Click += resetButton_Click;            
            return menuView;
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            this._menuClickControl.ResetClicks();
        }
    }
}