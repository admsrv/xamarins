﻿#define Weather2_5
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherCore
{
    public class Constants
    {
        #region Openweathermap2.5
#if Weather2_5
       public const string URL_OPENWEATHERMAPAPI = "http://api.openweathermap.org/data/2.5/";
       public const string APIQUERY_BYGEOCORD = "weather?lat={0}&lon={1}&units={2}&APPID={3}";
       public const string UNITS_METRIC = "metric";
       public const string UNITS_IMPERIAL = "imperial";
       public const string OPENWEATHER_API_KEY = "imperial";
       public const string OPENWEATHERMAP_KEY = "00444cc7c51e77bc3f199cc5fa9c7bb7";
#endif
        #endregion
    }
}
