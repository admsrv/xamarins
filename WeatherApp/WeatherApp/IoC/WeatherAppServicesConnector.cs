using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using WeatherApp.AL.Services.GeoService;
using System.Threading.Tasks;

namespace WeatherApp
{
    public delegate void AppInitDelegate();
    public class WeatherAppServicesConnector 
    {
        public bool IsGeoBound { get; set; }
        public Binder GeoBinder { get; set; }
        public GeoService GeoSVC { get; set; }
        public GeoServiceConnection GeoConnection { get; set; }
        public Messenger UiInbox { get; set; }

        public WeatherAppServicesConnector()
        {
            GeoSVC = new GeoService();
            GeoConnection = new GeoServiceConnection(this);
            this.IsGeoBound = false;
        }

        public async Task<bool> StartGeoSvc()
        {
            var t = new Func<bool>(() =>
            {
                Application.Context.StartService(new Intent("WeatherApp.AL.Services.GeoService.GeoService"));
                var a = Application.Context.BindService(new Intent("WeatherApp.AL.Services.GeoService.GeoService"), this.GeoConnection, Bind.AutoCreate);
                return a;
            });

            return await Task.Factory.StartNew<bool>(t);
        }      

        public void StopGeoSvc()
        {
            if (this.IsGeoBound == true)
            {
                if (GeoConnection != null)
                    Application.Context.UnbindService(GeoConnection);
               this.IsGeoBound = !Application.Context.StopService(new Intent("WeatherApp.AL.Services.GeoService.GeoService"));
            }           
        }        
    }
}