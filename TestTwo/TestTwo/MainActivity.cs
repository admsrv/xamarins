﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainActivity.cs" company="AltexSoft">
//  sergiy.chepur@altexsoft.com 
//  © 2015  
// </copyright>
// <summary>
//   Represents the main screen activity of Clicker app.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestTwo
{
    using System;
    using Android.App;
    using Android.Content;
    using Android.Runtime;
    using Android.Views;
    using Android.Widget;
    using Android.OS;

    /// <summary>
    ///  Represents the main screen activity of Clicker app.
    /// </summary>
    [Activity(Label = "Clicker", Icon = "@drawable/icon")]  
    public class MainActivity : Activity, IWorkerFunctionality
    {
        /// <summary>
        /// Const key for Main Activity bundle key-value pair
        /// </summary>
        public const string ClicksBundleKey = "ClicksCount";

        /// <summary>
        /// Const name of app-settings file
        /// </summary>
        public const string AppSettingsFilename = "ClickerAppSettings";

        /// <summary>
        /// Const TAG for menu fragment
        /// </summary>
        public const string MenuFragmentTag = "menu";

        /// <summary>
        /// Const TAG for Worker fragment
        /// </summary>
        public const string WorkerFragmentTag = "worker";
        

        /// <summary>
        /// Gets or sets <see cref="TextView"/> element which display current clicks count.
        /// </summary>
        public TextView DisplayClicksLabel { get; set; }

        /// <summary>
        /// Gets or sets user clicks count.
        /// </summary>
        public int ClicksCount { get; set; }

        /// <summary>
        /// Wide screen device app executing flag.
        /// </summary>
        public bool IsWideScreenLayout
        {
            get
            {
                var fragContainer = this.FindViewById<View>(Resource.Id.workercontainer);
                if (fragContainer != null && fragContainer.Visibility == ViewStates.Visible)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Indicates Is a worker fragment present on main layout.
        /// </summary>
        public bool IsWorkerLoaded 
        { 
            get
            {
                var fragContainer = this.FragmentManager.FindFragmentByTag(WorkerFragmentTag);
                if (fragContainer != null && fragContainer.IsVisible)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Update clicks count label.
        /// </summary>
        public void UpdateLabel()
        {
            this.DisplayClicksLabel.Text = string.Format("Click\'s count: {0}", this.ClicksCount);
        }

        /// <summary>
        /// Increase count of clicks one.
        /// </summary>
        public void IncClick()
        {
            this.ClicksCount++;
            this.UpdateLabel();
        }


        /// <summary>
        /// Reset count of clicks to zero
        /// </summary>
        public void ResetClicks()
        {
            this.ClicksCount = 0;
            if (this.IsWorkerLoaded)
            {
                this.UpdateLabel();
            }
        }

        /// <summary>
        /// Initialize by default or load application settings.
        /// </summary>
        /// <param name="bundleMenu">
        /// Bundle for <see cref="MenuFragment"/>
        /// </param>
        /// <param name="bundleApp">
        /// </param>
        private void InitSettings(out Bundle bundleMenu, out Bundle bundleApp)
        {
            var appSettings = GetSharedPreferences(AppSettingsFilename, Android.Content.FileCreationMode.Private);
            var menuBundle = new Bundle();
            var appBundle = new Bundle();
            if (!appSettings.All.ContainsKey(MenuFragment.HidenStateFlagConst))
            {
                this.InitSettingKey(MenuFragment.HidenStateFlagConst, ref appSettings);                              
            }
            
            menuBundle.PutBoolean(MenuFragment.HidenStateFlagConst, appSettings.GetBoolean(MenuFragment.HidenStateFlagConst, true));
            if (!appSettings.All.ContainsKey(ClicksBundleKey))
            {
                this.InitSettingKey(ClicksBundleKey, ref appSettings);  
            }

            appBundle.PutInt(ClicksBundleKey, appSettings.GetInt(ClicksBundleKey, 0));
            bundleMenu = menuBundle;
            bundleApp = appBundle;
        }

        /// <summary>
        /// Initializate a clicker settings by key.
        /// </summary>
        /// <param name="key">
        /// Settings value key
        /// </param>
        /// <param name="settingsContainer">
        /// Reference to application <see cref="SharedPreferences"/> settings container.
        /// </param>
        private void InitSettingKey(string key, ref ISharedPreferences settingsContainer)
        {
            var editor = settingsContainer.Edit();
            switch (key)
            {
                case MenuFragment.HidenStateFlagConst:
                    {
                        editor.PutBoolean(MenuFragment.HidenStateFlagConst, true);                        
                        break;
                    }

                case ClicksBundleKey:
                    {
                        editor.PutInt(ClicksBundleKey, 0);
                        break;
                    }
            }
            editor.Commit();
        }

        /// <summary>
        /// Insert worker fragment to container
        /// </summary>
        /// <param name="containerId">
        /// Container for clicker-worker fragment.
        /// </param>
        private void InsertWorkerFragmentTo(int containerId = Resource.Id.workercontainer)
        {
            var workerFragment = new WorkerFragment();
            var fragmentManagerTransaction = FragmentManager.BeginTransaction();
            fragmentManagerTransaction.Add(containerId, workerFragment, WorkerFragmentTag);
            fragmentManagerTransaction.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="containerId">
        /// Container for clicker-menu fragment.
        /// </param>
        private void InsertMenuFragmentTo(int containerId = Resource.Id.fragcontainer)
        {
            var menuFragment = new MenuFragment();
            var fragmentManagerTransaction = FragmentManager.BeginTransaction();
            fragmentManagerTransaction.Add(containerId, menuFragment, MenuFragmentTag);
            fragmentManagerTransaction.Commit();
        }

        /// <summary>
        /// Home button clicked handler
        /// </summary>
        private void OnHomeButtonClicked()
        {      
            switch (this.IsWideScreenLayout)
            {
                case true:
                    {
                        var menuFrag = this.FragmentManager.FindFragmentByTag(MenuFragmentTag);
                        if (menuFrag.IsVisible)
                        {
                            menuFrag.View.Visibility = ViewStates.Gone;
                            return;
                        }

                        menuFrag.View.Visibility = ViewStates.Visible;
                        break;
                    }

                case false:
                    {
                        this.SwapMenuAndWorker();
                        break;
                    }
            }
        }

        /// <summary>
        /// Swaps Menu and Worker fragment 
        /// </summary>
        private void SwapMenuAndWorker()
        {                            
          var workerFragment = this.FragmentManager.FindFragmentByTag(WorkerFragmentTag);         
          if (workerFragment != null && (workerFragment.IsVisible))
          {
              var n = new MenuFragment();
              this.FragmentManager.BeginTransaction().Replace(Resource.Id.fragcontainer,n,MenuFragmentTag).AddToBackStack(null).Commit();
          } else
          {
              this.FragmentManager.PopBackStack();
          }
        }

        /// <summary>
        /// Save click's count and menu visibility status to Shared Preferences
        /// </summary>
        public void SaveSettings()
        {
            var appSettings = GetSharedPreferences(AppSettingsFilename, Android.Content.FileCreationMode.Private);
            var settingsEdit = appSettings.Edit();
            settingsEdit.PutInt(ClicksBundleKey, this.ClicksCount);
            switch (IsWideScreenLayout)
            {
                case true:
                    {
                        var menuFrag = this.FindViewById<FrameLayout>(Resource.Id.fragcontainer);
                        if (menuFrag.Visibility != ViewStates.Visible)
                        {
                            settingsEdit.PutBoolean(MenuFragment.HidenStateFlagConst, true);
                        }
                        else
                        {
                            settingsEdit.PutBoolean(MenuFragment.HidenStateFlagConst, false);
                        }
                        break;
                    }
            }
           
            settingsEdit.Commit();
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            this.ActionBar.NavigationMode = ActionBarNavigationMode.Standard;
            this.ActionBar.SetDisplayShowHomeEnabled(true);
            if (bundle != null)
            {

                this.ClicksCount = bundle.GetInt(ClicksBundleKey);
                return;
            }

            var appBundle = new Bundle();
            var appMenuBundle = new Bundle();
            InitSettings(out appMenuBundle, out appBundle);
            base.OnCreate(appBundle);
            this.ClicksCount = appBundle.GetInt(ClicksBundleKey);

            if (IsWideScreenLayout)
            {
                this.InsertMenuFragmentTo();
                this.InsertWorkerFragmentTo();
            }
            else
            {
                this.InsertWorkerFragmentTo(Resource.Id.fragcontainer);
            }            
        }

        public override bool OnMenuItemSelected(int featureId, IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home: //// Home button pressed
                    {
                        this.OnHomeButtonClicked();
                        break;
                    }
            }
            return base.OnMenuItemSelected(featureId, item);
        }

        protected override void OnResume()
        {
            base.OnResume();
            switch (IsWideScreenLayout)
            {
                case true:
                    {
                        this.UpdateLabel();
                        break;
                    }

                case false:
                    {
                        if (IsWorkerLoaded)
                        {
                            this.UpdateLabel();
                        }
                        break;
                    }
            }
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            outState.PutInt(ClicksBundleKey, this.ClicksCount);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (!this.IsChangingConfigurations)
            {
                this.SaveSettings();
            }
        }

        protected override void OnRestoreInstanceState(Bundle savedInstanceState)
        {
            base.OnRestoreInstanceState(savedInstanceState);
            if (savedInstanceState!=null)
            {               
                this.ClicksCount = savedInstanceState.GetInt(ClicksBundleKey,0);
            }
        }
    }
}

