namespace cWeather
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    
    using Android.App;
    using Android.Content;
    using Android.OS;
    using Android.Runtime;
    using Android.Util;
    using Android.Views;
    using Android.Widget;
    using Android.Locations;

    public class MenuFragment : Fragment
    {
        private IWeatherCallback _mainActivityCallbacks;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var menuView = inflater.Inflate(Resource.Layout.layout_menu, container, false);
            if( savedInstanceState != null)
            {
                return menuView;
            }

            var gr = menuView.FindViewById<RadioGroup>(Resource.Id.accuracyChoseGroup);
            gr.Check(this.AccuracyStarupValue(this._mainActivityCallbacks.LocationCriteria));
            gr.CheckedChange += gr_CheckedChange;
            return menuView;
        }

        public override void OnAttach(Activity activity)
        {
            base.OnAttach(activity);
            if (activity is IWeatherCallback)
            {
                this._mainActivityCallbacks = (IWeatherCallback)activity;
            } else
            {
                throw new ArgumentException(string.Format("Activity {0} does not implement IWeatherCallback interface.", activity.ComponentName));
            }
        }

        void gr_CheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            var senderRadio = sender as RadioGroup;
            if (senderRadio != null)
            {
                switch (e.CheckedId)
                {
                    case Resource.Id.fineAccuracy:
                        {
                            this._mainActivityCallbacks.LocationCriteria.Accuracy = Accuracy.Fine;
                            break;
                        }                   

                    default:
                        {
                            this._mainActivityCallbacks.LocationCriteria.Accuracy = Accuracy.NoRequirement;
                            break;
                        }
                }
            }
        }

        private int AccuracyStarupValue(Criteria criteria)
        {
            if (criteria == null)
            {
                throw new ArgumentNullException("Location manager Criteria cannot be null.");
            }

            switch (criteria.Accuracy)
            {
               case Accuracy.Coarse:
                    {
                        return Resource.Id.coarseAccuracy;
                    }

               case Accuracy.Fine:
                    {
                        return Resource.Id.fineAccuracy;
                    }

                default:
                    {
                        throw new ArgumentException("Unknown accuracy");
                    }
            }
        }
    }
}