﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V4.Widget;
using WeatherApp.BL.ViewModels;
using Android.Util;
using Newtonsoft.Json;
using WeatherCore.Types._2dot5;
using WeatherApp.BL.Fragments;

namespace WeatherApp
{
    [Activity(Label = "WeatherApp", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private DrawerLayout drawerLayoutObject;
        private ListView drawerList;
        private Messenger _uiInbox;
        private WeatherAppServicesConnector _servicesConnector;
        private static readonly string[] menuSections = new[] { "Main", "Options", "Exit" };

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);            
            this.MenuInit();
            this.ContentInit();
            _servicesConnector = App.iocContainer.Resolve<WeatherAppServicesConnector>();
           // StartGeo();            
            if (bundle != null)
            {
                return;
            }            
        }

        private void ContentInit()
        {
            var mainF = new FragmentForecastsList();          
            var mainFragmentTransaction = this.FragmentManager.BeginTransaction();
            mainFragmentTransaction.Add(Resource.Id.contentcontainer, mainF, Constants.TAG_MAIN_FRAGMENT);
            mainFragmentTransaction.Commit();           
        }

        public async void StartGeo()
        {
            var result = await _servicesConnector.StartGeoSvc();
            if (result)
            {
                Log.Debug("________________________________________________", "success");
                this.RegisterUi();
                this.StartGeoSvc();
            }
        }

        private void RegisterUi()
        {
            this._uiInbox = new Messenger(new UiMessageHandler(this));                     
            var msg = Message.Obtain();
            msg.What = Constants.GEOSVC_MSG_CONNECTION_UI;
            msg.ReplyTo = this._uiInbox;
            this._servicesConnector.GeoConnection.GeoServiceInbox.Send(msg);
        }

        private void MenuInit()
        {
            this.drawerLayoutObject = this.FindViewById<DrawerLayout>(Resource.Id.rootDrawer);
            this.drawerList = this.FindViewById<ListView>(Resource.Id.left_drawer);
            this.drawerList.Adapter = new ArrayAdapter<string>(this, Resource.Layout.menu_list, menuSections);
            this.drawerList.ItemClick += drawerList_ItemClick;
        }

        void drawerList_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            switch (e.Position)
            {
                default:
                    {
                        break;
                    }
            }
        }

        private void StartGeoSvc()
        {
            var containerObject = App.iocContainer.Resolve<WeatherAppServicesConnector>();
            var msg = Message.Obtain();
            msg.What = Constants.GEOSVC_MSG_GEO_STARTLOCATION;
            var msgBundle = new Bundle();
            msgBundle.PutString(Constants.GEOSVC_KEY_LOCATOR_ACCURACY, Constants.GEOSVC_VALUE_LOCATOR_ACCURACY_FINE);
            msgBundle.PutString(Constants.GEOSVC_KEY_LOCATOR_POWER, Constants.GEOSVC_VALUE_LOCATOR_POWER_HIGH);
            msg.Data = msgBundle;
            containerObject.GeoConnection.GeoServiceInbox.Send(msg);
        }

        public void UpdateCurrentForecast(Bundle bundle)
        {
            if(bundle.ContainsKey(Constants.WEATHER_CURRENT_FORECAST))
            {
                var serializedObject = bundle.GetString(Constants.WEATHER_CURRENT_FORECAST);
                var ss = JsonConvert.DeserializeObject<RespondByGeoCoord>(serializedObject);
                
            }

            //throw new NotImplementedException();
        }
    }

    public class MenuItemClickListener : Java.Lang.Object, View.IOnClickListener
    {
        public void OnClick(View v)
        {
            throw new NotImplementedException();
        }
    }
}

