using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;
using Android.Util;

namespace WeatherApp.AL.Services.GeoService
{
    public class GeoServiceHandler : Handler
    {
        private GeoService _service;
        public GeoServiceHandler(GeoService svc)
        {
            this._service = svc;
            Log.Debug("______________________________", "Handle created");
        }        

        public override void HandleMessage(Message msg)
        {
            switch (msg.What)
            {
                case Constants.GEOSVC_MSG_GEO_GETLOCATION:
                    {
                        this._service.LocationRequest();
                        break;
                    }

                case Constants.GEOSVC_MSG_GEO_STARTLOCATION:
                    {                        
                        var locatorArgument = this.ConstructLocator(msg.Data);
                        this._service.StartListenLocator(locatorArgument);
                        break;
                    }

                case Constants.GEOSVC_MSG_GEO_STOPLOCATION:
                    {
                        this._service.StopListenLocator();
                        break;
                    }

                case Constants.GEOSVC_MSG_CONNECTION_UI:
                    {                       
                        this._service.UIMessanger = msg.ReplyTo;                        
                        break;
                    }

                default:
                    {
                        base.HandleMessage(msg);
                        break;
                    }
            }            
        }

        private Criteria ConstructLocator(Bundle bundle)
        {            
            if (bundle == null)
            {
                throw new ArgumentNullException("Bundle for ConstructLocator cannot be null");
            }

            if (!bundle.ContainsKey(Constants.GEOSVC_KEY_LOCATOR_ACCURACY) || !bundle.ContainsKey(Constants.GEOSVC_KEY_LOCATOR_POWER))
            {
                return new Criteria() { Accuracy = Accuracy.Coarse, PowerRequirement = Power.NoRequirement };
            }

           var criteriaToResult = new Criteria();
           GetAccuracy(bundle, ref criteriaToResult);
           GetPower(bundle, ref criteriaToResult);
           return criteriaToResult;
        }

        private static void GetAccuracy(Bundle bundle, ref Criteria criteriaToResult)
        {
            switch (bundle.GetString(Constants.GEOSVC_KEY_LOCATOR_ACCURACY))
            {
                case Constants.GEOSVC_VALUE_LOCATOR_ACCURACY_COARSE:
                    {
                        criteriaToResult.Accuracy = Accuracy.Coarse;
                        break;
                    }

                case Constants.GEOSVC_VALUE_LOCATOR_ACCURACY_FINE:
                    {
                        criteriaToResult.Accuracy = Accuracy.Fine;
                        break;
                    }

                default:
                    {
                        criteriaToResult.Accuracy = Accuracy.NoRequirement;
                        break;
                    }
            }
        }

        private static void GetPower(Bundle bundle, ref Criteria criteriaToResult)
        {
            switch (bundle.GetString(Constants.GEOSVC_KEY_LOCATOR_POWER))
            {
                case Constants.GEOSVC_VALUE_LOCATOR_POWER_LOW:
                    {
                        criteriaToResult.PowerRequirement = Power.Low;
                        break;
                    }

                case Constants.GEOSVC_VALUE_LOCATOR_POWER_HIGH:
                    {
                        criteriaToResult.PowerRequirement = Power.High;
                        break;
                    }

                default:
                    {                        
                        criteriaToResult.PowerRequirement = Power.NoRequirement;
                        break;
                    }
            }
        }
    }
}