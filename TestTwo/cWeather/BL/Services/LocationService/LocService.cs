using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;
using Android.Util;

namespace cWeather
{
    [Service]
    [IntentFilter(new string[]{"cWeather.LocService"})]
    public class LocService : Service, ILocationListener
    {
        private LocationManager LocMgr;
        public event EventHandler<LocationChangedEventArgs> LocationChanged;
        public event EventHandler<ProviderDisabledEventArgs> ProviderDisabled = delegate { };
        public event EventHandler<ProviderEnabledEventArgs> ProviderEnabled = delegate { };
        public event EventHandler<StatusChangedEventArgs> StatusChanged = delegate { };

        private LocBinder _tBinder;

        public override void OnCreate()
        {
            base.OnCreate();
            this.LocMgr = Android.App.Application.Context.GetSystemService("location") as LocationManager;
        }

        public void StartLocationUpdates()
        {
            var locationCriteria = new Criteria();
            locationCriteria.Accuracy = Accuracy.Fine;
            locationCriteria.PowerRequirement = Power.High;
            var locationProvider = LocMgr.GetBestProvider(locationCriteria, true);
            LocMgr.RequestLocationUpdates(locationProvider, 40000, 0, this);
        }

        public void OnLocationChanged(Android.Locations.Location location)
        {
            if (this.LocationChanged != null)
            {
                this.LocationChanged(this, new LocationChangedEventArgs(location));
            }
        }

        public void OnProviderDisabled(string provider)
        {
            this.ProviderDisabled(this, new ProviderDisabledEventArgs(provider));
        }

        public void OnProviderEnabled(string provider)
        {
            this.ProviderEnabled(this, new ProviderEnabledEventArgs(provider));
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {
            this.StatusChanged(this, new StatusChangedEventArgs(provider, status, extras));
        } 

        public override IBinder OnBind(Intent intent)
        {
           this._tBinder = new LocBinder(this);            
           return this._tBinder;
        }
    }
}