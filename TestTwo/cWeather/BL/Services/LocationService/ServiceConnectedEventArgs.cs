using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using cWeather;
using Android.Locations;

namespace cWeather
{
  public delegate void ServiceConnectedHandler(object sender, ServiceConnectedEventArgs s);
  public class ServiceConnectedEventArgs : EventArgs
    {
        public IBinder Binder { get; set; }
    }
}