using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using WeatherAPP.AL.Services.GeoService;
using System.Threading.Tasks;

namespace WeatherAPP
{
 
    public class WeatherAppServicesConnector 
    {
        public bool IsGeoBound { get; set; }
        public Binder GeoBinder { get; set; }
        public GeoService GeoSVC { get; set; }
        public GeoServiceConnection GeoConnection { get; set; }

        public WeatherAppServicesConnector()
        {
            GeoSVC = new GeoService();
            GeoConnection = new GeoServiceConnection(this);
        }

        public void StartGeoSvc()
        {
        new Task(() =>
        {
        Application.Context.StartService(new Intent("WeatherAPP.AL.Services.GeoService.GeoService"));
        Application.Context.BindService(new Intent("WeatherAPP.AL.Services.GeoService.GeoService"), GeoConnection, Bind.AutoCreate);}
        ).Start();
        }

        public void kkkk() { }

        public void StopLocationService()
        {
            if (GeoConnection != null)
                Application.Context.UnbindService(GeoConnection);
            //if (GeoSVC != null)
            //    GeoSVC.StopSelf();

            Application.Context.StopService(new Intent("WeatherAPP.AL.Services.GeoService.GeoService"));
        }
    }
}