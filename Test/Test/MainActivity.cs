﻿namespace Test
{
    using System;
    using Android.App;
    using Android.Content;
    using Android.Runtime;
    using Android.Views;
    using Android.Widget;
    using Android.OS;

    [Activity(Label = "Test", Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
       protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            ActionBar.NavigationMode = ActionBarNavigationMode.Standard;
            ActionBar.SetHomeButtonEnabled(true);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetTitle(Resource.String.MainPanelText);            
        }

        public override bool OnOptionsItemSelected (IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home: 
                    {
                        new AlertDialog.Builder(this).SetMessage("Clicked home").Create().Show(); 
                        break; 
                    }
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}

