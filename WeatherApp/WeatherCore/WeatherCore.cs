﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using WeatherCore.Types._2dot5;
using System.Net;
using System.IO;
using System.Net.Http;


namespace WeatherCore
{
    public class WeatherCore
    {
        public void test()
        {
            var a = WeatherQuery<RespondByGeoCoord>(string.Format("{0}{1}", Constants.URL_OPENWEATHERMAPAPI, string.Format(Constants.APIQUERY_BYGEOCORD, 49.05991979, 33.41766357, "8b00bf90efc63e8e941fd2976827b00a")));
        }

        public T WeatherQuery<T>(string queryUrl) where T : new()
        {
            if (string.IsNullOrWhiteSpace(queryUrl))
            {
                throw new ArgumentException("Incorrect url string. URL cannot be null, empty or whitespace.");
            }

            try
            {
                var wRequest = (HttpWebRequest)HttpWebRequest.Create(new Uri(queryUrl));
                wRequest.ContentType = "application/json";
                wRequest.Method = "GET";
                using (var response = wRequest.GetResponse())
                {

                    if (response.ContentLength > 0)
                    {
                        using (var responseStream = response.GetResponseStream())
                        {
                            var jsonSerializer = new DataContractJsonSerializer(typeof(T));
                            var deserializedObject = (T)jsonSerializer.ReadObject(responseStream);
                            return deserializedObject;
                        }
                    }

                    return new T();
                }
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Weather Api query {0} fails with message: {1}", typeof(T).ToString(), e.Message), e);
            }
        }

        public WeatherCore()
        {
            // TODO: Complete member initialization
        }
    }
}
