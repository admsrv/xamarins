using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace WeatherApp.BL.Fragments
{
    public class FragmentForecastsList : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var forecastsView = inflater.Inflate(Resource.Layout.layout_weather, container, false);
            var fragmentCurrentForecast = new FragmentCurrentForecast();
            var transaction = this.FragmentManager.BeginTransaction();
            transaction.Add(Resource.Id.currentforecastcontainer, fragmentCurrentForecast, Constants.TAG_CURRENTFORECAST_FRAGMENT);
            transaction.Commit();
            return forecastsView;
        }
    }
}