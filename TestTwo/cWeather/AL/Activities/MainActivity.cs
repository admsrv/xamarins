﻿namespace cWeather
{
    using System;
    using Android.App;
    using Android.Content;
    using Android.Runtime;
    using Android.Views;
    using Android.Widget;
    using Android.OS;
    using Android.Locations;
    using Android.Util;
    using Android.Content.Res;

    [Activity(Label = "Weather test app", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity, IWeatherCallback
    {
        public bool IsBound { get; set; }
        

        public Criteria LocationCriteria { get; set; }
        public bool IsWideScreenLayout { get; set; }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);            
            try
            {
                SetContentView(Resource.Layout.Main);
                ActionBar.NavigationMode = ActionBarNavigationMode.Standard;
                ActionBar.SetHomeButtonEnabled(true);
                if (bundle != null)
                {
                    return;
                }
               
                this.LocationCriteria = new Criteria() { Accuracy = Accuracy.Coarse, PowerRequirement = Power.Medium };
                this.IsWideScreenLayout = this.IsWideLayout();
                this.InitFragments();
            } 
            catch(Exception e)
            {
#if DEBUG
                Log.Error("Error in OnCreate or called them methods: ", e.Message);
#endif
                throw e;
            }
        }

        protected override void OnStart()
        {           
            base.OnStart();
            var v = FindViewById<Button>(Resource.Id.refreshweather);
            v.Click += v_Click;
        }

        void v_Click(object sender, EventArgs e)
        {
            App.LocationServiceConnected += App_LocationServiceConnected;           
            App.StartS();
            
        }

        void App_LocationServiceConnected(object sender, ServiceConnectedEventArgs e)
        {
            ((LocBinder)e.Binder).Service.LocationChanged += _LocService_LocationChanged;
        }

        void _LocService_LocationChanged(object sender, LocationChangedEventArgs e)
        {
            var v = FindViewById<Button>(Resource.Id.refreshweather);
            RunOnUiThread(() => { v.Text = e.Location.Latitude.ToString() + " " + e.Location.Longitude.ToString(); });
        }

        public override bool OnMenuItemSelected(int featureId, IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    {
                        this.OnHomeButtonClicked();
                        break;
                    }
            }

            return base.OnMenuItemSelected(featureId, item);
        }


        private void InitFragments()
        {
            switch (this.IsWideScreenLayout)
            {
                case true:
                    {
                        this.InsertFragmentTo(new MenuFragment(), Constants.MenuFragmentTag, Resource.Id.fragmentcontainer);
                        this.InsertFragmentTo(new WorkareaFragment(), Constants.WorkareaFragmentTag, Resource.Id.workercontainer);
                        break;
                    }

                case false:
                    {
                        this.InsertFragmentTo(new WorkareaFragment(), Constants.WorkareaFragmentTag, Resource.Id.fragmentcontainer);
                        break;
                    }
            }
        }

        private bool IsWideLayout()
        {
            if (Resources.Configuration.ScreenLayout.HasFlag(ScreenLayout.SizeLarge) ||
                Resources.Configuration.ScreenLayout.HasFlag(ScreenLayout.SizeXlarge))
            {
                return true;
            }

            return false;
        }

        private void OnHomeButtonClicked()
        {
            switch (this.IsWideScreenLayout)
            {
                case true:
                    {
                        var menuFrag = this.FragmentManager.FindFragmentByTag(Constants.MenuFragmentTag);
                        this.FragmentShowHide(menuFrag);
                        break;
                    }

                case false:
                    {
                        this.SwapMenuAndWorker();
                        break;
                    }
            }
        }

        private void FragmentShowHide(Fragment fragmentToPerform, bool? visible = null)
        {
            var fragmentsTransaction = this.FragmentManager.BeginTransaction();
            try
            {
                if (visible == null)
                {
                    FragmentVisibilitySwitcher(fragmentToPerform, ref fragmentsTransaction, fragmentToPerform.IsVisible);
                }
                else
                {
                    FragmentVisibilitySwitcher(fragmentToPerform, ref fragmentsTransaction, visible.Value);
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Log.Error("Main activity error in FragmentShowHide: ", e.Message);
#endif
                throw new Exception("Main activity error in FragmentShowHide", e);
            }
            finally
            {
                fragmentsTransaction.Commit();
            }
        }

        private static void FragmentVisibilitySwitcher(Fragment menuFragment, ref FragmentTransaction fragmentsTransaction, bool visible)
        {
            switch (visible)
            {
                case true:
                    {
                        fragmentsTransaction.Hide(menuFragment);
                        break;
                    }

                case false:
                    {
                        fragmentsTransaction.Show(menuFragment);
                        break;
                    }
            }
        }

        private void SwapMenuAndWorker()
        {
            var workerFragment = this.FragmentManager.FindFragmentByTag(Constants.WorkareaFragmentTag);
            if (workerFragment != null && (workerFragment.IsVisible))
            {
                var n = new MenuFragment();
                this.FragmentManager.BeginTransaction().Replace(Resource.Id.fragmentcontainer, n, Constants.MenuFragmentTag).AddToBackStack(null).Commit();
            }
            else
            {
                this.FragmentManager.PopBackStack();
            }
        }

        private void InsertFragmentTo(Fragment fragmentToPerform, string fragmentTag, int fragmentContainerId = Resource.Id.fragmentcontainer)
        {
            try
            {
                if (string.IsNullOrEmpty(fragmentTag) || string.IsNullOrWhiteSpace(fragmentTag))
                {
                    throw new ArgumentException("Fragment tag cannot be null, empty or whitespace.");
                }

                if (fragmentToPerform == null)
                {
                    throw new ArgumentException("Fragment cannot be null.");
                }

                if (FindViewById(fragmentContainerId) == null)
                {
                    throw new ArgumentException("Container resource cannot be found.");
                }

                var fragmentManagerTransaction = FragmentManager.BeginTransaction();
                fragmentManagerTransaction.Add(fragmentContainerId, fragmentToPerform, fragmentTag);
                fragmentManagerTransaction.Commit();
            }
            catch (Exception e)
            {
#if DEBUG
                Log.Error("Error in InsertFragmentTo: ", e.Message);
#endif
                throw new Exception(string.Format("Error in InsertFragmentTo: {0}", e.Message), e);
            }
        }
    }
}

