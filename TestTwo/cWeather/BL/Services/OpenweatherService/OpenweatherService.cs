using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace cWeather.BL.Services.OpenweatherService
{
    [Service]
    [IntentFilter(new string[]{"cWeather.BL.Services.OpenweatherService"})]
    public class OpenweatherService : Service
    {
        private OpenweatherServiceBinder _WeatherBinder;
        public override IBinder OnBind(Intent intent)
        {
            this._WeatherBinder = new OpenweatherServiceBinder(this);
            return this._WeatherBinder;
        }

        
    }
}