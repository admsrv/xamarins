﻿
using Android.OS;
using System;
using WeatherCore.Types._2dot5;
using System.Linq;

namespace WeatherApp.AL.Services.GeoService
{
    public static class WeatherDataFactory
    {
        public static Bundle Construct(RespondByGeoCoord respond)
        {
            var bundleResult = new Bundle();
            if (respond == null)
            {
                throw new ArgumentNullException("respond object is null");
            }
            
            return bundleResult;            
        }
    }
}