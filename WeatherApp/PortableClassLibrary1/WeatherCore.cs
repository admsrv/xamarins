﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using WeatherCore.Types._2dot5;
using System.Net;
using System.IO;
using RestSharp.Portable;
using System.Net.Http;
using RestSharp.Portable.Deserializers;
using Newtonsoft.Json;



namespace WeatherCore
{
    public class WeatherCore
    {
        public async Task<string> GetWeatherByCoordinates(double? latitude, double? longitude, string apiKey)
        {           
            if (latitude == null || longitude == null)
            {
                throw new ArgumentNullException("Some coordinate is null.");
            }

            if (string.IsNullOrWhiteSpace(apiKey))
            {
                throw new ArgumentException("ApiKey cannot be null, empty or whitespace.");
            }

            try
            {
                var resClient = new RestClient(Constants.URL_OPENWEATHERMAPAPI);
                var resRequest = new RestRequest(string.Format(Constants.APIQUERY_BYGEOCORD, latitude, longitude, Constants.UNITS_METRIC, apiKey), HttpMethod.Get);
                var deserial = new JsonDeserializer();
                var response = await resClient.Execute(resRequest);                      
                var currentWeatherRespond = deserial.Deserialize<RespondByGeoCoord>(response);
                var tmp = JsonConvert.SerializeObject(currentWeatherRespond);
                return tmp;                
            }
            catch (Exception e)
            {
                throw new OperationCanceledException(string.Format("GetWeatherByCoordinates fails with message: {0}", e.Message), e);
            }
        }       
    }
}
