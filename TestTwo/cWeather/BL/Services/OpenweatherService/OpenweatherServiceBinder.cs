using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace cWeather.BL.Services.OpenweatherService
{
    public class OpenweatherServiceBinder : Binder
    {
        private OpenweatherService openweatherService;

        public OpenweatherServiceBinder(OpenweatherService openweatherService)
            {
               this.openweatherService = openweatherService;
            }            
    }
}