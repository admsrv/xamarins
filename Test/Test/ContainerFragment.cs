using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Test
{
    public class ContainerFragment : Fragment
    {
        public bool IsWideScreen 
        {
            get
            {
               var workerView = Activity.FindViewById(Resource.Id.workerframe);
                return (workerView == null)? false: (workerView.Visibility == ViewStates.Visible);
            }
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
   
            // Create your fragment here
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.MenuLayout, container);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
           // if (this.IsWideScreen) 
        }
    }
}