using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;


namespace cWeather
{    
    [Application]
    public class App : Application
    {
        public static LocConnection _LocConnection;
        public static LocBinder _LocBinder;
        public static LocService _LocService;

        public static event EventHandler<ServiceConnectedEventArgs> LocationServiceConnected;

        public App(IntPtr handle, JniHandleOwnership transfer)
            : base(handle,transfer)
        {
            _LocService = new LocService();
            _LocBinder = new LocBinder(_LocService);
            _LocConnection = new LocConnection(_LocBinder);
            _LocConnection.ServiceConnected += _LocConnection_ServiceConnected;
        }

        void _LocConnection_ServiceConnected(object sender, ServiceConnectedEventArgs s)
        {
            var ss = (LocBinder)s.Binder;
            LocationServiceConnected(this, s);
            ss.Service.StartLocationUpdates();
        }
        
        public static void StartS()
        {
            new Task(
                () =>
                {
                    Android.App.Application.Context.StartService(new Intent("cWeather.LocService"));
                    Android.App.Application.Context.BindService(new Intent("cWeather.LocService"), _LocConnection, Bind.AutoCreate);
                }
                ).Start();
        }

        public static void StopLocationService()
        {
            if (_LocConnection != null)
                Android.App.Application.Context.UnbindService(_LocConnection);
            if (_LocConnection != null)
                Android.App.Application.Context.StopService(new Intent("cWeather.LocService"));
        }

        public override void OnCreate()
        {
            base.OnCreate();
        }        
    }
}