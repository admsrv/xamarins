using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using WeatherAPP.AL.Services.GeoService;
using System.Threading.Tasks;

namespace WeatherAPP
{
    [Application]
    public class App : Application
    {
        public static TinyIoC.TinyIoCContainer SS;
        public App(IntPtr handle, JniHandleOwnership transfer)
            : base(handle, transfer)
        {
            SS = new TinyIoC.TinyIoCContainer();
            SS.Register<WeatherAppServicesConnector>().AsSingleton();
        }

        public override void OnCreate()
        {
            base.OnCreate();
        }
    }
}