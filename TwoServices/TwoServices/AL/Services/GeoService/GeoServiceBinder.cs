using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace WeatherAPP.AL.Services.GeoService
{
    public class GeoServiceBinder : Binder
    {
        private GeoService _bindedGeoService;
        public GeoService BindedGeoService
        {
            get
            {
                return this._bindedGeoService;
            }
        }

        public GeoServiceBinder(GeoService gsvc)
        {
            if (gsvc == null)
            {
                throw new ArgumentNullException("Binder: Geoservice cannot be null.");
            }

            this._bindedGeoService = gsvc;
        }
    }
}