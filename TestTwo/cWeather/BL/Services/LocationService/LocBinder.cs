using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;

namespace cWeather
{
    public class LocBinder : Binder
    {
        private LocService _service;
        public bool IsBound { get; set; }
        public LocService Service { get { return this._service; } }

        public LocBinder(LocService serv)
        {
            this._service = serv;
            this.IsBound = false;
        } 
    }
}