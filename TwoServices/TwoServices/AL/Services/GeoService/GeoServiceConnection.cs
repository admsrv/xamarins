using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace WeatherAPP.AL.Services.GeoService
{
    public class GeoServiceConnection : Java.Lang.Object, IServiceConnection
    {        
        public WeatherAppServicesConnector AppExtended;
        public bool IsBound { get; set; }
        public Messenger GeoServiceInbox;
        public GeoServiceConnection(WeatherAppServicesConnector appParam)
        {
            this.AppExtended = appParam;
            this.AppExtended.IsGeoBound = false;
        }

        public void OnServiceConnected(ComponentName name, IBinder service)
        {            
            var svcBinder = service as Binder;
            if (svcBinder == null)
            {
                throw new ArgumentNullException("Binder cannot be null");
            }

            this.AppExtended.GeoBinder = svcBinder;
            this.GeoServiceInbox = new Messenger(svcBinder);
            this.AppExtended.IsGeoBound = true;            
        }

        public void OnServiceDisconnected(ComponentName name)
        {
            this.AppExtended.IsGeoBound = false;           
        }
    }
}