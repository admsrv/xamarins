namespace EducationProjectChapterTwo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;

    using Android.App;
    using Android.Content;
    using Android.OS;
    using Android.Runtime;
    using Android.Views;
    using Android.Widget;

    [Activity(Label = "Splash Screen", MainLauncher = true, NoHistory = true)]
    public class SplashScreenActivity : Activity
    {
        private Thread dethThread;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.SplashScreen);
            this.dethThread = new Thread(delegate() 
                {
                    Thread.Sleep(5000);
                    StartActivity(typeof(MainActivity));
                });
            this.dethThread.Start();
        }
    }
}