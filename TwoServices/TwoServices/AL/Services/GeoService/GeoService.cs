using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;
using Android.Util;
using Java.Util;

namespace WeatherAPP.AL.Services.GeoService
{
    [Service]
    [IntentFilter(new String[] { "WeatherAPP.AL.Services.GeoService.GeoService" })]
    public class GeoService : Service, ILocationListener
    {
        private Messenger SvcMessanger;        
        private LocationManager LocMgr;
        private string locationProvider;
        private string CurrentCity;
        private long LastLocalForecast;

        public GeoService()
        {
            this.SvcMessanger = new Messenger(new GeoServiceHandler(this));
        }        

        //_______________________________
        

        public Location LocationRequest()
        {
            return null;
        }

        public void StartListenLocator(Criteria criteriaConfig, float distance = 3000, long minTime = 60000 )
        {
            if (criteriaConfig == null)
            {
                throw new ArgumentNullException("Location criteria cannot be null.");
            }

            this.LocMgr = GetSystemService(Context.LocationService) as LocationManager;            
            this.locationProvider = this.LocMgr.GetBestProvider(criteriaConfig, true);            
            switch (this.locationProvider)
            {
                case Constants.PROVIDER_GPS:
                    {
                       // this.LocMgr.RequestLocationUpdates(locationProvider, minTime, distance, this, null);                        
                        this.LocMgr.RequestLocationUpdates(locationProvider, 1000, 0, this, null);                        
                        break;
                    }
                case Constants.PROVIDER_NETWORK:
                    {
                       // this.LocMgr.RequestLocationUpdates(locationProvider, minTime, 0, this);
                        this.LocMgr.RequestLocationUpdates(locationProvider, 1000, 0, this);
                        break;
                    }
            }
            
        }        

        public void StopListenLocator()
        {

        }       

        public override IBinder OnBind(Intent intent)
        {
            return SvcMessanger.Binder;
        }

        public override bool OnUnbind(Intent intent)
        {
            return base.OnUnbind(intent);
        }

        public void OnLocationChanged(Location location)
        {
            this.CityChanged(location);
            if (this.WeatherSvcMessanger != null)
            {                
                if (this.CityChanged(location))
                {
                    //this.WeatherSvcMessanger.Send(MessageBuilder.Build(Constants.));
                }

                if (this.ItsTimeToLocalUpdate)
                { 

                }
            }
        }

        private bool CityChanged(Location location)
        {            
            var geoCoder = new Geocoder(this);
            bool d = Geocoder.IsPresent;
            var adress = geoCoder.GetFromLocation(location.Latitude, location.Longitude, 10);
            if (adress != null && adress.Count() > 0)
            {
                var city = adress[0].Locality;
            }
            throw new NotImplementedException();
        }

        public void OnProviderDisabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnProviderEnabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {
            throw new NotImplementedException();
        }

        public Messenger WeatherSvcMessanger { get; set; }

        public Messenger UIMessanger { get; set; }

        public bool ItsTimeToLocalUpdate { get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }
    }
}