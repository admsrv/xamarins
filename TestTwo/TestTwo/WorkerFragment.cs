namespace TestTwo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Android.App;
    using Android.Content;
    using Android.OS;
    using Android.Runtime;
    using Android.Util;
    using Android.Views;
    using Android.Widget;

    public class WorkerFragment : Fragment
    {
        private IWorkerFunctionality _activityMethodsCallback;        

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var workerView = inflater.Inflate(Resource.Layout.layout_worker, container, false);
            this._activityMethodsCallback.DisplayClicksLabel = workerView.FindViewById<TextView>(Resource.Id.tickdisplaytext);
            var incButton = workerView.FindViewById(Resource.Id.inctickbutton);
            incButton.Click += this.incButton_Click;
            return workerView;
        }

        public override void OnAttach(Activity activity)
        {
            base.OnAttach(activity);
            if (activity is IWorkerFunctionality)
            {
                this._activityMethodsCallback = (IWorkerFunctionality)activity; 
            }
        }

        public override void OnResume()
        {
            base.OnResume();
            this._activityMethodsCallback.UpdateLabel();
        }

        private void incButton_Click(object sender, EventArgs e)
        {
            if (this._activityMethodsCallback != null)
            {
                this._activityMethodsCallback.IncClick();
            }
        }
    }
}