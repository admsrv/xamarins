namespace TestTwo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;

    using Android.App;
    using Android.Content;
    using Android.OS;
    using Android.Runtime;
    using Android.Views;
    using Android.Widget;

    [Activity(Label = "Test project chapter 2.1", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {
        private const int threadLifetime = 5000;
        private Thread _lifetimeThread;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            this.SetContentView(Resource.Layout.layout_splash);
            ThreadStart threadBody = delegate()
            {
                Thread.Sleep(threadLifetime);
                StartActivity(typeof(MainActivity));
            };

            this._lifetimeThread = new Thread(threadBody);
            this._lifetimeThread.Start();
        }
    }
}