﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace WeatherCore.Types._2dot5
{
    [DataContract]
    public class Coord
    {
        [JsonProperty(PropertyName = "lon")]
        public double Rongitude { get; set; }

        [JsonProperty(PropertyName = "lat")]
        public double Latitude { get; set; }
    }

    [DataContract]
    public class Sys
    {
        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }

        [JsonProperty(PropertyName = "sunrise")]
        public int Sunrise { get; set; }

        [JsonProperty(PropertyName = "sunset")]
        public int Sunset { get; set; }
    }

    [DataContract]
    public class Weather
    {
        [JsonProperty(PropertyName = "id")]
        public int WeatherConditionId { get; set; }

        [JsonProperty(PropertyName = "main")]
        public string MainWeatherParams { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string WeatherDescription { get; set; }

        [JsonProperty(PropertyName = "icon")]
        public string WeatherIcon { get; set; }
    }

    [DataContract]
    public class Main
    {
        [JsonProperty(PropertyName = "temp")]
        public double Temperature { get; set; }

        [JsonProperty(PropertyName = "humidity")]
        public int Humidity { get; set; }

        [JsonProperty(PropertyName = "pressure")]
        public double AtmosphericPressure { get; set; }

        [JsonProperty(PropertyName = "temp_min")]
        public double MinimumTemperature { get; set; }

        [JsonProperty(PropertyName = "temp_max")]
        public double MaximumTemperature { get; set; }
    }

    [DataContract]
    public class Wind
    {
        [JsonProperty(PropertyName = "speed")]
        public double WindSpeed { get; set; }

        [JsonProperty(PropertyName = "deg")]
        public double WindDirectionDegree { get; set; }
    }

    [DataContract]
    public class Rain
    {
        //[DataMember(Name = "3h")]
        [JsonProperty(PropertyName="3h")]
        public double ThreeH { get; set; }
    }

    [DataContract]
    public class Clouds
    {
        [JsonProperty(PropertyName = "all")]
        public int CloudinessPercent { get; set; }
    }

    [DataContract]
    public class RespondByGeoCoord
    {
        [JsonProperty(PropertyName = "coord")]
        public Coord Coordinates { get; set; }

        [JsonProperty(PropertyName = "sys")]
        public Sys sys { get; set; }

        [DataMember]
        public IList<Weather> weather { get; set; }

        [JsonProperty(PropertyName = "main")]
        public Main main { get; set; }

        [JsonProperty(PropertyName = "wind")]
        public Wind wind { get; set; }

        [JsonProperty(PropertyName = "rain")]
        public Rain rain { get; set; }

        [JsonProperty(PropertyName = "clouds")]
        public Clouds clouds { get; set; }

        [JsonProperty(PropertyName = "dt")]
        public double TimeOfData { get; set; }

        [JsonProperty(PropertyName = "id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }

        [JsonProperty(PropertyName = "cod")]
        public int cod { get; set; }
    }
}
