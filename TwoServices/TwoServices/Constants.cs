using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace WeatherAPP
{
    public static class Constants
    {

        public const int GEOSVC_MSG_CONNECTION_WEATHER = 1;
        public const int GEOSVC_MSG_CONNECTION_UI = 2;
        public const int WEATHERSVC_MSG_CONNECTION_GEO = 3;
        public const int WEATHERSVC_MSG_CONNECTION_UI = 4;

        //_______________________________________

        public const int GEOSVC_MSG_GEO_STARTLOCATION = 5;
        public const int GEOSVC_MSG_GEO_STOPLOCATION = 6;
        public const int GEOSVC_MSG_GEO_GETLOCATION = 7;
        //_______________________________________
        
        public const string KEY_LOCATOR_ACCURACY = "accuracy";
        public const string VALUE_LOCATOR_ACCURACY_COARSE = "coarse";
        public const string VALUE_LOCATOR_ACCURACY_FINE = "fine";

        public const string KEY_LOCATOR_POWER = "power";        
        public const string VALUE_LOCATOR_POWER_LOW = "low";
        public const string VALUE_LOCATOR_POWER_HIGH = "high";

        public const string KEY_LOCATOR_MINTIME = "mintime";
        public const string KEY_LOCATOR_MINDISTANCE = "mindistance";
        //_______________________________________

        public const string PROVIDER_GPS = "gps";
        public const string PROVIDER_NETWORK = "network";
        //_______________________________________

        public const string APP_KEY = "AIzaSyATXXDgLKws60y-XGj6mEdbplT9aL0Ac0Y";
    }
}